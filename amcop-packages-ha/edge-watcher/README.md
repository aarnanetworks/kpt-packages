# edge-watcher

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] edge-watcher`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree edge-watcher`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init edge-watcher
kpt live apply edge-watcher --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
