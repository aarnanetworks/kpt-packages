# ib-network

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] ib-network`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree ib-network`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init ib-network
kpt live apply ib-network --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
