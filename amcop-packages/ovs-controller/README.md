# ovs-controller

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] ovs-controller`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree ovs-controller`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init ovs-controller
kpt live apply ovs-controller --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
