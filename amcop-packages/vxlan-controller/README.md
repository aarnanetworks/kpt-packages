# vxlan-controller

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] vxlan-controller`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree vxlan-controller`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init vxlan-controller
kpt live apply vxlan-controller --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
