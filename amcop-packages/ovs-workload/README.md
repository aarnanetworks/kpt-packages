# ovs-workload

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] ovs-workload`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree ovs-workload`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init ovs-workload
kpt live apply ovs-workload --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
