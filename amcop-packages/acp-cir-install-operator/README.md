# acp-cir-install-operator

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] acp-cir-install-operator`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree acp-cir-install-operator`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init acp-cir-install-operator
kpt live apply acp-cir-install-operator --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
