# tenant-fmpm

## Description
This package contains deployment resources like below in a one deployment with multi-container
        * fmpm
        * smo-handler
        * vm-provisioner
## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] tenant-fmpm`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree tenant-fmpm`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init tenant-fmpm
kpt live apply tenant-fmpm --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
