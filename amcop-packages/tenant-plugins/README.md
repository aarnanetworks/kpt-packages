# tenant-plugins

## Description
This package contains deployment resources like below in a one deployment with multi-container
        * rand-acp
        * cn-raemis
        * dmp
        * ocnos-csr

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] tenant-plugins`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree tenant-plugins`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init tenant-plugins
kpt live apply tenant-plugins --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
