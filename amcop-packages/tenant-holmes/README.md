# tenant-holmes

## Description
This package contains deployment resources like below in a one deployment with multi-container
        * holmes
        * pgpool
        * ves-collector

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] tenant-holmes`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree tenant-holmes`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init tenant-holmes
kpt live apply tenant-holmes --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
