# tenant-analytics

## Description
This package contains deployment resources like below in a one deployment with multi-container
        * kafka
        * dmaap
        * zookeeper
        * kafka-rest-proxy


## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] tenant-analytics`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree tenant-analytics`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init tenant-analytics
kpt live apply tenant-analytics --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
